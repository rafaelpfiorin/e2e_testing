# Front-end test automation - End to end

## How to use it

1. Clone this project with HTTPS option.
2. Set up the software to be tested into the folder named application, following the respective 'README.md'.
3. Unzip the '1. Test cases writing - Front-end Web' file to read the test cases written about it.
4. Unzip the '2. Test cases automation - Front-end Web' file to configure the project and execute the automated tests of the previous step (there's a README.md too).

Enjoy!